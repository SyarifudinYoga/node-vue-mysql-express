import Vue from 'vue'
import VueRouter from 'vue-router'
 
import App from './App.vue'
import Create from './components/AddPendidikan.vue'
import Edit from './components/EditPendidikan.vue'
import Index from './components/PendidikanList.vue'
 
Vue.use(VueRouter)
 
Vue.config.productionTip = false
//untuk mengatur
const routes = [
  {
    name: 'Create',
    path: '/pendidikan/add',
    component: Create
  },
  {
    name: 'Edit',
    path: '/pendidikan/update/:id',
    component: Edit
  },
  {
    name: 'Index',
    path: '/',
    component: Index
  },
];
 
const router = new VueRouter({ mode: 'history', routes: routes })

new Vue({
  // init router
  router,
  render: h => h(App),
}).$mount('#app')