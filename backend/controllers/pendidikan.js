// Import function from Product Model
import { getPendidikan, getPendidikanById, insertPendidikan, updatePendidikanById, deletePendidikanById } from "../models/pendidikanModel.js";

// Get All Products
export const showPendidikan = (req, res) => {
    getPendidikan((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Get Single Product 
export const showPendidikanById = (req, res) => {
    getPendidikanById(req.params.id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Create New Product
export const createPendidikan = (req, res) => {
    const data = req.body;
    insertPendidikan(data, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Update Product
export const updatePendidikan = (req, res) => {
    const data  = req.body;
    const id    = req.params.id;
    updatePendidikanById(data, id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Delete Product
export const deletePendidikan = (req, res) => {
    const id = req.params.id;
    deletePendidikanById(id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}