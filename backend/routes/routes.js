// import express
import express from "express";

// import function from controller
import { showPendidikan, showPendidikanById, createPendidikan, updatePendidikan, deletePendidikan } from "../controllers/pendidikan.js";

// init express router
const router = express.Router();

// Get All Product
router.get('/pendidikan/view', showPendidikan);

// Get Single Product
router.get('/pendidikan/view/:id', showPendidikanById);

// Create New Product
router.post('/pendidikan/add', createPendidikan);

// Update Product
router.put('/pendidikan/update/:id', updatePendidikan);

// Delete Product
router.delete('/pendidikan/delete/:id', deletePendidikan);

// export default router
export default router;