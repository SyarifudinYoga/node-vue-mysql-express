// import connection
import db from "../config/database.js";

// Get All Products
export const getPendidikan = (result) => {
    db.query("SELECT * FROM tb_pendidikan", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Get Single Product
export const getPendidikanById = (id, result) => {
    db.query("SELECT * FROM tb_pendidikan WHERE kode_pendidikan = ?", [id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results[0]);
        }
    });   
}

// Insert Product to Database
export const insertPendidikan = (data, result) => {
    db.query("INSERT INTO tb_pendidikan SET ?", [data], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Update Product to Database
export const updatePendidikanById = (data, id, result) => {
    db.query("UPDATE tb_pendidikan SET nama_pendidikan = ? WHERE kode_pendidikan = ?", [data.nama_pendidikan, id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Delete Product to Database
export const deletePendidikanById = (id, result) => {
    db.query("DELETE FROM tb_pendidikan WHERE kode_pendidikan = ?", [id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}